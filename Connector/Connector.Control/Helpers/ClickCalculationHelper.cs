﻿using System;

namespace Connector.Control.Helpers
{
    public static class ClickCalculationHelper
    {
        private const int clickPerPersonCap = 5;
        private const float clicksPerSecondPerConnectionThreshehold = 0.25F;

        public static bool CalculateThresheholdWasMet(int secondsSinceLastCount, int clicksFromAllConnections, int connections)
        {
            if (connections == 0)
                return false;

            var cappedClicks = Math.Min(clicksFromAllConnections, clickPerPersonCap * connections);

            float clicksPerSecondPerConnection = (cappedClicks / connections) / secondsSinceLastCount;

            return clicksPerSecondPerConnection > clicksPerSecondPerConnectionThreshehold;
        }
    }
}
