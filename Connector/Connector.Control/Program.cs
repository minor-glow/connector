using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Serilog;
using Serilog.Events;

namespace Connector.Control
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var defaultLogLevel = LogEventLevel.Debug;
            var overrideLogEventLevel = LogEventLevel.Information;
            var consoleLogLevel = LogEventLevel.Debug;

            var logConfiguration = new LoggerConfiguration()
                .MinimumLevel.Is(defaultLogLevel)
                .MinimumLevel.Override("Microsoft", overrideLogEventLevel)
                .MinimumLevel.Override("Microsoft.AspNetCore", overrideLogEventLevel)
                .Enrich.FromLogContext()
                .WriteTo.Console(restrictedToMinimumLevel: consoleLogLevel);

            Log.Logger = logConfiguration.CreateLogger();

            CreateHostBuilder(args).Build().Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder
                        .UseStartup<Startup>()
                        .UseSerilog();
                });
    }
}
