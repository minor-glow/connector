﻿using System.Threading;
using System.Threading.Tasks;

namespace Connector.Control.Business
{
    public class ClickCounter
    {
        private bool connected;
        private CancellationTokenSource cancellationTokenSource;
        private CancellationToken cancellationToken;
        private readonly IClickStateManager stateManager;

        public ClickCounter(IClickStateManager stateManager)
        {
            this.stateManager = stateManager;

            Connect();
            RestartDisconnectTimeout();
        }

        public void Count()
        {
            Connect();

            stateManager.Count();
            RestartDisconnectTimeout();
        }

        public void Disconnect()
        {
            if(connected)
            {
                stateManager.DisconnectCounter();
                connected = false;
            }
        }

        private void Connect()
        {
            if(!connected)
            {
                stateManager.ConnectCounter();
                connected = true;
            }
        }

        private void RestartDisconnectTimeout()
        {
            cancellationTokenSource?.Cancel();

            cancellationTokenSource = new CancellationTokenSource();
            cancellationToken = cancellationTokenSource.Token;

            Task.Run(async () =>
            {
                await Task.Delay(Constants.CounterTimeout, cancellationToken);
                Disconnect();
            }, cancellationToken);
        }
    }
}
