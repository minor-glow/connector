﻿using Connector.Control.Model;
using System;

namespace Connector.Control.Business
{
    public interface IClickStateManager
    {
        void Count();
        void ConnectCounter();
        void DisconnectCounter();

        int CalculateIntensity(DateTimeOffset timestamp);
        ClickState GetState();
    }
}