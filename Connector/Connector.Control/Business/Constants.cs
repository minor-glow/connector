﻿using System;

namespace Connector.Control.Business
{
    public static class Constants
    {
        public static TimeSpan ClickCountInterval => TimeSpan.FromSeconds(1);
        public static TimeSpan CounterTimeout => TimeSpan.FromSeconds(10);
    }
}
