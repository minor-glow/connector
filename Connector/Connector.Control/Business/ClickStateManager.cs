﻿using Connector.Control.Helpers;
using Connector.Control.Model;
using System;

namespace Connector.Control.Business
{
    public class ClickStateManager : IClickStateManager
    {
        private int intensityPercentage = 10;
        private const int intensityPercentageStepper = 10;

        public int currentTotalClickCount;
        private int connectionsCount;
        private DateTimeOffset lastIntensityCalculation = DateTimeOffset.Now;

        public void ConnectCounter() => connectionsCount = Math.Min(int.MaxValue, connectionsCount + 1);
        public void DisconnectCounter() => connectionsCount = Math.Max(0, connectionsCount - 1);
        public void Count() => currentTotalClickCount = Math.Min(int.MaxValue, currentTotalClickCount + 1);

        public int CalculateIntensity(DateTimeOffset timestamp)
        {
            var timeSinceLastRead = timestamp - lastIntensityCalculation;

            var thresheholdWasMet = ClickCalculationHelper.CalculateThresheholdWasMet(
                secondsSinceLastCount: Math.Min(int.MaxValue, (int)timeSinceLastRead.TotalSeconds),
                clicksFromAllConnections: currentTotalClickCount,
                connections: connectionsCount);

            if (thresheholdWasMet)
                intensityPercentage = Math.Min(intensityPercentage + intensityPercentageStepper, 100);
            else
                intensityPercentage = Math.Max(intensityPercentage - intensityPercentageStepper / 2, 10);

            lastIntensityCalculation = timestamp;
            currentTotalClickCount = 0;

            return intensityPercentage;
        }

        public ClickState GetState()
            => new ClickState
            {
                CurrentIntensity = intensityPercentage,
                ConnectionsCount = connectionsCount,
                CurrentTotalClickCount = currentTotalClickCount,
                LastIntensityCalculation = lastIntensityCalculation
            };
    }
}
