﻿using System;

namespace Connector.Control.Model
{
    public class ClickState
    {
        public int CurrentIntensity { get; set; }
        public int CurrentTotalClickCount { get; set; }
        public int ConnectionsCount { get; set; }
        public DateTimeOffset LastIntensityCalculation { get; set; }
    }
}
