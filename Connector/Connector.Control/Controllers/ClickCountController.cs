﻿using Connector.Control.Business;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Connector.Control.Controllers
{
    [Route("clicks")]
    [ApiController]
    public class ClickCountController : ControllerBase
    {
        private readonly IClickStateManager clickStateManager;
        private readonly string superSecretToken = "533b3124e0a387abd814e6c83537f164b2472ed9e82c5acf400159139c47be117f462b5e14d0fa478d837e8a7dd8225a";
        
        public ClickCountController(IClickStateManager clickStateManager)
        {
            this.clickStateManager = clickStateManager;
        }

        [HttpGet]
        [Route("count")]
        public IActionResult CountClicks()
        {
            if (HttpContext.Request.Headers["x-token"] != superSecretToken)
                return Forbid("Incorrect token!");

            return Ok(clickStateManager.CalculateIntensity(DateTimeOffset.Now));
        }
    }
}
